//
// Created by illfate on 1.05.20.
//

#ifndef PIPELINE_NON_RESTORING_DIVISION_BINARYNUMBER_H
#define PIPELINE_NON_RESTORING_DIVISION_BINARYNUMBER_H


#include <vector>
#include <algorithm>

const int SIZE = 4;

class BinaryNumber {
public:
    explicit BinaryNumber(int n) {
        data_number = toBinary(n);
    }

    explicit BinaryNumber(const std::vector<int> num) {
        data_number = num;
    }

    BinaryNumber(const BinaryNumber &number) {
        data_number = number.data_number;
    }

    BinaryNumber &operator=(const BinaryNumber &number) {
        data_number = number.data_number;
        return *this;
    }

    [[nodiscard]] std::vector<int> GetBinary() const {
        return data_number;
    }

    [[nodiscard]] std::vector<int> GetInvertedBinary() const {
        std::vector<int> result;
        result.resize(4);
        for (int i = 0; i < data_number.size(); ++i) {
            result[i] = !data_number[i];
        }
        return result;
    }

    BinaryNumber Add(const BinaryNumber &num) {
        std::vector<int> sum(SIZE, 0);

        int carry = 0;
        for (int i = 0; i < SIZE; i++) {
            sum[i] = ((data_number[i] ^ num.data_number[i]) ^ carry);
            carry = ((data_number[i] & num.data_number[i]) | (data_number[i] & carry)) | (num.data_number[i] & carry);
        }

        BinaryNumber res(0);
        res.data_number = sum;
        this->data_number = sum;
        return res;
    }

    BinaryNumber Subtract(const BinaryNumber &num) {
        auto addNum = num.GetAdditionalBinary();
        auto additionalBinary = BinaryNumber(addNum);
        this->data_number = Add(additionalBinary).data_number;
        return *this;
    }


    void SetFirstBit(bool bit) {
        data_number[0] = bit;
    }

    void SetLastBit(bool bit) {
        data_number[SIZE-1] = bit;
    }

    int GetFirstBit() const {
        return data_number[0];
    }

    int GetLastBit() const {
        return data_number[SIZE-1];
    }

    [[nodiscard]] std::vector<int> GetAdditionalBinary() const {
        BinaryNumber one(0);
        one.data_number = ONE;
        BinaryNumber res(0);
        res.data_number = GetInvertedBinary();
        return res.Add(one).GetBinary();
    }

    [[nodiscard]] int64_t GetDecimal() const {
        return toDecimal();
    }

    int GetNumberOfActualBits() const {
        for (auto it = data_number.rbegin(); it != data_number.rend(); ++it) {
            if (*it == 1) {
                return data_number.rend() - it;
            }
        }
        return 0;
    }

    void Log(const std::string &name, std::ostream &os) const {
        os << name << ": " << NormalOutput() << std::endl;
    }

    BinaryNumber LeftShift() {
        std::vector<int> temp = std::vector<int>(data_number.begin(), data_number.end() - 1);
        temp.emplace(temp.begin(), 0);
        data_number = temp;
        return *this;
    }

    BinaryNumber LeftShift(BinaryNumber &num) {
        LeftShift();
        data_number[0] = num.GetLastBit();
        num.LeftShift();
        return *this;
    }

    BinaryNumber RightShift() {
        std::vector<int> temp = std::vector<int>(next(data_number.begin()), data_number.end());
        temp.push_back(0);
        data_number = temp;
        return *this;
    }

    std::string ToString() const {
        std::string res;
        for (auto x: data_number) {
            res += std::to_string(x);
        }
        return res;
    }

    std::string NormalOutput() const {
        auto res = ToString();
        return {res.rbegin(), res.rend()};
    }

private:
    std::vector<int> data_number;
    const std::vector<int> ONE = {1, 0, 0, 0};

    static std::vector<int> toBinary(int n) {
        int mod;
        std::vector<int> result;

        for (int i = 0; n > 0; i++) {
            mod = n % 2;
            n = (n - mod) / 2;
            result.push_back(mod);
        }

        for (int i = 0; i < result.size() - 4; ++i) {
            result.push_back(0);
        }
        return std::move(result);
    }

    [[nodiscard]] int64_t toDecimal() const {
        int64_t result = 0;
        for (size_t i = 0; i < data_number.size(); ++i) {
            if (data_number[i] == 1) {
                result += pow(2, i);
            }
        }
        return result;
    }
};


#endif //PIPELINE_NON_RESTORING_DIVISION_BINARYNUMBER_H
