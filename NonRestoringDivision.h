//
// Created by illfate on 1.05.20.
//

#ifndef PIPELINE_NON_RESTORING_DIVISION_NONRESTORINGDIVISION_H
#define PIPELINE_NON_RESTORING_DIVISION_NONRESTORINGDIVISION_H


#include "BinaryNumber.h"
#include <iostream>

struct DivisionResult {
    BinaryNumber quotient;
    BinaryNumber remainder;
};

class NonRestoringDivision {
public:
    NonRestoringDivision(BinaryNumber divisor, BinaryNumber divided) : divisor(divisor), dividend(divided) {

    }

    DivisionResult Process() {
        int numBits = dividend.GetNumberOfActualBits();
        BinaryNumber temp(0);
        for (int i = 0; i < numBits; ++i) {
            processFirstStep(temp);
            processSecondStep(temp);
        }
        if (temp.GetLastBit()) {
            temp.Add(divisor);
        }
        return {dividend, temp};
    }



private:
    BinaryNumber divisor;
    BinaryNumber dividend;

    void processFirstStep(BinaryNumber &temp) {
        if (!temp.GetLastBit()) {
            temp.LeftShift(dividend);
            temp.Subtract(divisor);
        } else {
            temp.LeftShift(dividend);
            temp.Add(divisor);
        }
    }

    void processSecondStep(BinaryNumber &temp) {
        if (!temp.GetLastBit()) {
            dividend.SetFirstBit(1);
        } else {
            dividend.SetFirstBit(0);
        }
    }
};


#endif //PIPELINE_NON_RESTORING_DIVISION_NONRESTORINGDIVISION_H
