#include <iostream>

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do
// this in one cpp file
#include "catch.hpp"
#include "BinaryNumber.h"
#include "NonRestoringDivision.h"


// Задание
// алгоритм вычисления целочисленного частного пары 4-разрядных чисел делением без восстановления частичного остатка.

template<typename T>
bool operator==(const std::vector<T> &lhs, const std::vector<T> &rhs) {
    if (lhs.size() != rhs.size()) {
        return false;
    }
    for (size_t i = 0; i < lhs.size(); ++i) {
        if (lhs[i] != rhs[i]) {
            return false;
        }
    }
    return true;
}



TEST_CASE("Check division") {
    BinaryNumber dividend(11);
    BinaryNumber divisor(3);

    NonRestoringDivision div(divisor, dividend);
    auto[quotient, remainder]=div.Process();
    REQUIRE(quotient.GetDecimal() == 3);
    REQUIRE(remainder.GetDecimal() == 2);
}

TEST_CASE("Check division2") {
    BinaryNumber dividend2(11);
    BinaryNumber divisor2(2);

    NonRestoringDivision div2(divisor2, dividend2);
    auto[quotient2, remainder2]=div2.Process();
    REQUIRE(quotient2.GetDecimal() == 5);
    REQUIRE(remainder2.GetDecimal() == 1);
}

TEST_CASE("Check division4") {
    BinaryNumber dividend2(12);
    BinaryNumber divisor2(2);

    NonRestoringDivision div2(divisor2, dividend2);
    auto[quotient2, remainder2]=div2.Process();
    REQUIRE(quotient2.GetDecimal() == 6);
    REQUIRE(remainder2.GetDecimal() == 0);
}

TEST_CASE("Check division3") {
    BinaryNumber dividend(0);
    BinaryNumber divisor(3);

    NonRestoringDivision div(divisor, dividend);
    auto[quotient, remainder]=div.Process();
    REQUIRE(quotient.GetDecimal() == 0);
    REQUIRE(remainder.GetDecimal() == 0);
}



TEST_CASE("Left shift of two") {
    BinaryNumber first(0);
    BinaryNumber last(11);
    first.LeftShift(last);
    REQUIRE(first.GetDecimal() == 1);
    REQUIRE(last.GetDecimal() == 6);
}

TEST_CASE("Left shift of two2") {
    BinaryNumber first(0);
    BinaryNumber last(5);
    first.LeftShift(last);
    REQUIRE(first.GetDecimal() == 0);
    REQUIRE(last.GetDecimal() == 10);
}


TEST_CASE("Check converting to binary") {
    REQUIRE(BinaryNumber(5).GetBinary() == std::vector<int>{1, 0, 1, 0});
    REQUIRE(BinaryNumber(7).GetBinary() == std::vector<int>{1, 1, 1, 0});
    REQUIRE(BinaryNumber(15).GetBinary() == std::vector<int>{1, 1, 1, 1});
    REQUIRE(BinaryNumber(0).GetBinary() == std::vector<int>{0, 0, 0, 0});
}

TEST_CASE("Check actual bits") {
    REQUIRE(BinaryNumber(5).GetNumberOfActualBits() == 3);
    REQUIRE(BinaryNumber(1).GetNumberOfActualBits() == 1);
    REQUIRE(BinaryNumber(0).GetNumberOfActualBits() == 0);
    REQUIRE(BinaryNumber(3).GetNumberOfActualBits() == 2);
    REQUIRE(BinaryNumber(7).GetNumberOfActualBits() == 3);
    REQUIRE(BinaryNumber(8).GetNumberOfActualBits() == 4);
}

TEST_CASE("Check converting to inverted binary") {
    REQUIRE(BinaryNumber(5).GetInvertedBinary() == std::vector<int>{0, 1, 0, 1});
    REQUIRE(BinaryNumber(7).GetInvertedBinary() == std::vector<int>{0, 0, 0, 1});
    REQUIRE(BinaryNumber(15).GetInvertedBinary() == std::vector<int>{0, 0, 0, 0});
}

TEST_CASE("Check addition") {
    REQUIRE(BinaryNumber(5).BinaryNumber(BinaryNumber(5)).GetDecimal() == 10);
    REQUIRE(BinaryNumber(1).BinaryNumber(BinaryNumber(5)).GetDecimal() == 6);
    REQUIRE(BinaryNumber(10).BinaryNumber(BinaryNumber(5)).GetDecimal() == 15);
    REQUIRE(BinaryNumber(12).BinaryNumber(BinaryNumber(3)).GetDecimal() == 15);
}

TEST_CASE("Check substraction") {
    auto bin = BinaryNumber(1);
    bin.Subtract(BinaryNumber(3));
    REQUIRE(bin.GetBinary() == std::vector<int>{0, 1, 1, 1});
    REQUIRE(BinaryNumber(5).BinaryNumber(BinaryNumber(5)).GetDecimal() == 0);
    REQUIRE(BinaryNumber(10).BinaryNumber(BinaryNumber(5)).GetDecimal() == 5);
    REQUIRE(BinaryNumber(10).BinaryNumber(BinaryNumber(0)).GetDecimal() == 10);
    REQUIRE(BinaryNumber(10).BinaryNumber(BinaryNumber(15)).GetBinary() == std::vector<int>{1, 1, 0, 1});
}

TEST_CASE("Check converting to additional code binary") {
    REQUIRE(BinaryNumber(5).GetAdditionalBinary() == std::vector<int>{1, 1, 0, 1});
    REQUIRE(BinaryNumber(4).GetAdditionalBinary() == std::vector<int>{0, 0, 1, 1});
    REQUIRE(BinaryNumber(15).GetAdditionalBinary() == std::vector<int>{1, 0, 0, 0});
}

TEST_CASE("Check converting to decimal") {
    REQUIRE(BinaryNumber(5).GetDecimal() == 5);
    REQUIRE(BinaryNumber(7).GetDecimal() == 7);
    REQUIRE(BinaryNumber(15).GetDecimal() == 15);
    REQUIRE(BinaryNumber(0).GetDecimal() == 0);
}

TEST_CASE("Check left shift") {
    REQUIRE(BinaryNumber(5).LeftShift().GetDecimal() == 10);
    REQUIRE(BinaryNumber(7).LeftShift().GetDecimal() == 14);
    REQUIRE(BinaryNumber(15).LeftShift().GetDecimal() == 14);
    REQUIRE(BinaryNumber(0).LeftShift().GetDecimal() == 0);
}

TEST_CASE("Check right shift") {
    REQUIRE(BinaryNumber(5).RightShift().GetDecimal() == 2);
    REQUIRE(BinaryNumber(7).RightShift().GetDecimal() == 3);
    REQUIRE(BinaryNumber(15).RightShift().GetDecimal() == 7);
    REQUIRE(BinaryNumber(14).RightShift().GetDecimal() == 7);
    REQUIRE(BinaryNumber(12).RightShift().GetDecimal() == 6);
    REQUIRE(BinaryNumber(0).RightShift().GetDecimal() == 0);
}

TEST_CASE("Check string") {
    REQUIRE(BinaryNumber(5).ToString() == "1010");
}